package com.ekn.getpackage.homeassignment.data.network.contract

import com.ekn.getpackage.homeassignment.data.network.NetworkResponse
import com.ekn.getpackage.homeassignment.model.VersionResult

interface IVersionService {
    suspend fun actualAppVersion(): NetworkResponse<VersionResult>
}