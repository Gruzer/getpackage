package com.ekn.getpackage.homeassignment.ui.login

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.ekn.getpackage.homeassignment.databinding.FragmentLoginBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect


@AndroidEntryPoint
class LoginFragment : Fragment() {
    val loginViewModel: LoginViewModel by viewModels()
    lateinit var binding: FragmentLoginBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lifecycleScope.launchWhenStarted {
            loginViewModel.screenState.collect {
                render(it)
            }
        }

        binding.loginScreenLoginBtn.setOnClickListener {
            loginViewModel.obtainIntent(
                LoginScreenIntent.Auth(
                    binding.loginScreenEmailEdTxt.editableText.toString(),
                    binding.loginScreenPasswordEdTxt.editableText.toString()
                )
            )
        }


        binding.loginScreenEmailEdTxt.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                enableSignInBtn(validate())
            }

        })

        binding.loginScreenPasswordEdTxt.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                enableSignInBtn(validate())
            }

        })
    }

    private fun getEmail(): String {
        return binding.loginScreenEmailEdTxt.editableText.toString()
    }

    private fun getPassword(): String {
        return binding.loginScreenPasswordEdTxt.editableText.toString()
    }

    private fun validate(email: String, password: String): Boolean {
        return email.contains("@") && email.length > 5 && !email.startsWith("@") && !email.endsWith(
            "@"
        ) && password.length > 7
    }

    private fun validate(): Boolean {
        return validate(getEmail(), getPassword())
    }

    private fun enableSignInBtn(enable: Boolean) {
        binding.loginScreenLoginBtn.isEnabled = enable
    }


    private fun render(state: LoginScreenState) {
        when (state) {
            LoginScreenState.DONE -> {
                setEnableState(true)
                enableSignInBtn(validate())
            }
            is LoginScreenState.Error -> showError(state.message)
            LoginScreenState.LOGIN_IN_PROGRESS -> {
                setEnableState(false)
                enableSignInBtn(false)
            }
            LoginScreenState.NAVIGATE_MAIN_SCREEN -> navigateToMainScreen()
            LoginScreenState.NONE -> {
                setEnableState(true)
                enableSignInBtn(validate())
            }
        }
    }

    private fun setEnableState(enable: Boolean) {
        binding.loginScreenEmailEdTxt.isEnabled = enable
        binding.loginScreenPasswordEdTxt.isEnabled = enable
    }

    private fun navigateToMainScreen() {
        findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToMainScreenFragment())
    }

    private fun showError(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }


    companion object {

        @JvmStatic
        fun newInstance() =
            LoginFragment()

    }
}