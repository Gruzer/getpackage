package com.ekn.getpackage.homeassignment.model

sealed class AuthInfo {
    data class Data(val token: String, val time: Long) : AuthInfo()
    data class Error(val message: String, val code: Int) : AuthInfo()


}