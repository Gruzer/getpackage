package com.ekn.getpackage.homeassignment.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ekn.getpackage.homeassignment.data.repository.IMainScreenRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@HiltViewModel
class MainScreenViewModel
@Inject constructor(val repository: IMainScreenRepository) : ViewModel() {

    private val _screenState: MutableStateFlow<MainScreenState> =
        MutableStateFlow(MainScreenState.NONE)
    val screenState: StateFlow<MainScreenState>
        get() = _screenState

    init {
        obtainSessionInfo()
    }

    fun obtainIntent(intent: MainScreenIntent) {
        when (intent) {
            MainScreenIntent.LOGOUT -> performLogout()
        }
    }

    private fun performLogout() {
        viewModelScope.launch {
            _screenState.value = MainScreenState.IN_PROGRESS
            if (repository.logout()) {
                _screenState.value = MainScreenState.DONE
                _screenState.value = MainScreenState.LOGOUT
            } else {
                _screenState.value = MainScreenState.DONE
                _screenState.value = MainScreenState.Error("Fail To Logout")
            }
        }
    }

    private fun obtainSessionInfo() {
        if (screenState.value != MainScreenState.IN_PROGRESS)
            viewModelScope.launch {
                _screenState.value = MainScreenState.IN_PROGRESS
                delay(100)
                handleSessionInfo(repository.getSessionInitTime())
                _screenState.value = MainScreenState.DONE
            }
    }

    private fun handleSessionInfo(time: Long) {
        val date = getDate(time, "dd/MM/yyyy hh:mm:ss")
        _screenState.value = MainScreenState.SessionInfo(date)
    }

    private fun getDate(milliSeconds: Long, dateFormat: String): String {
        // Create a DateFormatter object for displaying date in specified format.
        val formatter = SimpleDateFormat(dateFormat)

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        val calendar: Calendar = Calendar.getInstance()
        calendar.timeInMillis = milliSeconds
        return formatter.format(calendar.time)
    }
}