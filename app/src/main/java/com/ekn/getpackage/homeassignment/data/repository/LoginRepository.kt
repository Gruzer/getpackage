package com.ekn.getpackage.homeassignment.data.repository

import com.ekn.getpackage.homeassignment.data.local.ISessionManager
import com.ekn.getpackage.homeassignment.data.network.NetworkResponse
import com.ekn.getpackage.homeassignment.data.network.contract.ILoginService
import com.ekn.getpackage.homeassignment.model.AuthInfo
import kotlinx.coroutines.delay

class LoginRepository constructor(
    private val service: ILoginService,
    private val sessionManager: ISessionManager
) : ILoginRepository {

    override suspend fun auth(email: String, password: String): AuthInfo {
        delay(1000)
        return when (val result = service.auth(email = email, password = password)) {
            is NetworkResponse.Failure -> AuthInfo.Error(
                result.message,
                result.statusCode ?: 0
            )
            is NetworkResponse.Success -> AuthInfo.Data(
                token = result.data?.token ?: "",
                time = updateSession(result.data?.token ?: "")
            )
            else -> AuthInfo.Error("General Problem", -1)
        }
    }

    private suspend fun updateSession(token: String): Long {
        sessionManager.storeToken(token)
        return sessionManager.getSessionInitTime()
    }

}