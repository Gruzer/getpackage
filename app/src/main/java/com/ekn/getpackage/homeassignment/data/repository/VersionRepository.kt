package com.ekn.getpackage.homeassignment.data.repository

import com.ekn.getpackage.homeassignment.data.network.NetworkResponse
import com.ekn.getpackage.homeassignment.data.network.contract.IVersionService
import com.ekn.getpackage.homeassignment.model.VersionInfo
import kotlinx.coroutines.delay

class VersionRepository constructor(private val service: IVersionService) : IVersionRepository {
    override suspend fun getActualAppVersion(): VersionInfo {
        delay(500)
        return when (val result = service.actualAppVersion()) {
            is NetworkResponse.Failure -> VersionInfo.Error(result.message, result.statusCode?:404)
            is NetworkResponse.Success -> VersionInfo.Data(result.data?.version?:0)
        }
    }
}