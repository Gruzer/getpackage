package com.ekn.getpackage.homeassignment.data.repository

import com.ekn.getpackage.homeassignment.data.local.ISessionManager

class MainScreenRepository constructor(private val sessionManager: ISessionManager) : IMainScreenRepository {


    override suspend fun logout(): Boolean {
        return sessionManager.closeSession()
    }

    override suspend fun getSessionInitTime(): Long {
        return sessionManager.getSessionInitTime()
    }
}