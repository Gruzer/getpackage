package com.ekn.getpackage.homeassignment.model

sealed class VersionInfo {
    data class Data(val version: Long) : VersionInfo()
    data class Error(val message: String, val code: Int) : VersionInfo()
}
