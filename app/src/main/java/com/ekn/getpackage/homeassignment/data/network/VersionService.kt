package com.ekn.getpackage.homeassignment.data.network

import com.ekn.getpackage.homeassignment.data.network.contract.IVersionService
import com.ekn.getpackage.homeassignment.model.AuthResult
import com.ekn.getpackage.homeassignment.model.VersionResult
import java.util.*
import kotlin.random.Random

class VersionService : IVersionService {
    override suspend fun actualAppVersion(): NetworkResponse<VersionResult> {
        val num = Random.nextInt(0, 100)
        return if (num > 20) {
            NetworkResponse.Success(VersionResult(2))
        } else {
            NetworkResponse.Success(VersionResult(-2))
        }

    }
}