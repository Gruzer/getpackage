package com.ekn.getpackage.homeassignment.ui.splash

sealed class SplashScreenState {
    object NONE : SplashScreenState()
    object WAIT : SplashScreenState() //version check
    object READY : SplashScreenState() //ready to move to login/main screen
    object SHOW_LOGIN_SCREEN : SplashScreenState()
    object SHOW_MAIN_SCREEN : SplashScreenState()
    object SHOW_VERSSION_ERROR_DIALOG : SplashScreenState()
    data class NetworkError(val message: String) : SplashScreenState()
}
