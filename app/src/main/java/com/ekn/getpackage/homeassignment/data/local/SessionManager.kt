package com.ekn.getpackage.homeassignment.data.local

import android.content.Context
import android.content.Context.MODE_PRIVATE

class SessionManager constructor(context: Context) : ISessionManager {

    private val preferences = context.getSharedPreferences("preferences", MODE_PRIVATE)
    private val KEY_TOKEN = "token"
    private val KEY_TIME = "time"


    override suspend fun storeToken(token: String): Boolean {
        preferences.edit().putString(KEY_TOKEN, token).apply()
        preferences.edit().putLong(KEY_TIME, System.currentTimeMillis()).apply()
        return true
    }


    override suspend fun getToken(): String {
        return preferences.getString(KEY_TOKEN, "") ?: ""
    }

    override suspend fun getSessionInitTime(): Long {
        return preferences.getLong(KEY_TIME, 0)
    }

    override suspend fun closeSession(): Boolean {
        preferences.edit().clear().apply()
        return true
    }

}