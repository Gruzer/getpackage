package com.ekn.getpackage.homeassignment.ui.login

sealed class LoginScreenState {
    object NONE : LoginScreenState()
    object LOGIN_IN_PROGRESS : LoginScreenState()
    object DONE : LoginScreenState()
    object NAVIGATE_MAIN_SCREEN : LoginScreenState()
    data class Error(val message: String) : LoginScreenState()

}
