package com.ekn.getpackage.homeassignment.application

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class HomeAssimentApp: Application() {
}