package com.ekn.getpackage.homeassignment.ui.login

sealed class LoginScreenIntent {
    data class Auth(val email: String, val password: String) : LoginScreenIntent()
}
