package com.ekn.getpackage.homeassignment.ui.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ekn.getpackage.homeassignment.data.repository.ILoginRepository
import com.ekn.getpackage.homeassignment.model.AuthInfo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val repository: ILoginRepository
) : ViewModel() {

    private val _screenState: MutableStateFlow<LoginScreenState> =
        MutableStateFlow(LoginScreenState.NONE)
    val screenState: StateFlow<LoginScreenState>
        get() = _screenState

    fun obtainIntent(intent: LoginScreenIntent) {
        when (intent) {
            is LoginScreenIntent.Auth -> performLogin(intent.email, intent.password)
        }
    }

    private fun performLogin(email: String, password: String) {
        if (_screenState.value != LoginScreenState.LOGIN_IN_PROGRESS)
            viewModelScope.launch {
                _screenState.value = LoginScreenState.LOGIN_IN_PROGRESS
                handleResult(repository.auth(email, password))

            }
    }

    private fun handleResult(result: AuthInfo) {
        _screenState.value = LoginScreenState.DONE
        when (result) {
            is AuthInfo.Data -> navigateToMainScreen()
            is AuthInfo.Error -> showError(result.message)
        }
    }

    private fun showError(message: String) {
        _screenState.value = LoginScreenState.Error(message)
    }

    private fun navigateToMainScreen() {
        _screenState.value = LoginScreenState.NAVIGATE_MAIN_SCREEN
    }

}