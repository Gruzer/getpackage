package com.ekn.getpackage.homeassignment.data.local

interface ISessionManager {
    suspend fun storeToken(token: String): Boolean
    suspend fun getToken(): String
    suspend fun getSessionInitTime(): Long
    suspend fun closeSession(): Boolean
}