package com.ekn.getpackage.homeassignment.data.network

sealed class NetworkResponse<out T>{
    data class Success<T>(val data: T?): NetworkResponse<T>()
    data class Failure(val statusCode: Int?,val message:String): NetworkResponse<Nothing>()
}
