package com.ekn.getpackage.homeassignment.ui.splash

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.ekn.getpackage.homeassignment.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


@AndroidEntryPoint
class SplashScreenFragment : Fragment() {
    val splashViewModel: SplashViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_splash_screen, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleScope.launchWhenStarted {
            launch {
                splashViewModel.performInit()
                splashViewModel.screenState.collect {
                    render(it)
                }
            }
        }
    }

    private fun render(state: SplashScreenState) {
        when (state) {
            SplashScreenState.NONE -> {
                //init state
                // Toast.makeText(context, "NONE", Toast.LENGTH_SHORT).show()
            }
            SplashScreenState.READY -> {
                // version done checking and ready to move to next screen
            }
            SplashScreenState.WAIT -> {
                //wait animation for version check
            }
            is SplashScreenState.NetworkError -> errorMessage(state.message)
            SplashScreenState.SHOW_LOGIN_SCREEN -> navigateToLoginScreen()
            SplashScreenState.SHOW_MAIN_SCREEN -> navigateToMainScreen()
            SplashScreenState.SHOW_VERSSION_ERROR_DIALOG -> versionErrorDialog()
        }
    }


    private fun navigateToLoginScreen() {
        findNavController().navigate(
            SplashScreenFragmentDirections.actionSplashScreenFragmentToLoginFragment()
        )
    }

    private fun navigateToMainScreen() {
        findNavController().navigate(
            SplashScreenFragmentDirections.actionSplashScreenFragmentToMainScreenFragment()
        )

    }

    private fun versionErrorDialog() {
        AlertDialog.Builder(requireContext())
            .setTitle(getString(R.string.version_dialog_error_message))
            .setNegativeButton(getString(R.string.version_dialog_error_negative_btn)) { _, _ -> activity?.finish() }
            .create().show()
    }

    private fun errorMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    companion object {

        @JvmStatic
        fun newInstance() =
            SplashScreenFragment()
    }
}