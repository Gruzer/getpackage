package com.ekn.getpackage.homeassignment.ui.main

sealed class MainScreenState {
    data class SessionInfo(val time: String) : MainScreenState()
    data class Error(val message: String) : MainScreenState()
    object NONE : MainScreenState()
    object IN_PROGRESS : MainScreenState()
    object DONE : MainScreenState()
    object LOGOUT : MainScreenState()
}
