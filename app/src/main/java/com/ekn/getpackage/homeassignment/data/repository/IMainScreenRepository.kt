package com.ekn.getpackage.homeassignment.data.repository

interface IMainScreenRepository {
    suspend fun logout(): Boolean
    suspend fun getSessionInitTime(): Long
}