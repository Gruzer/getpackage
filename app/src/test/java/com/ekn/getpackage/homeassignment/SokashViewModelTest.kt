package com.ekn.getpackage.homeassignment

import com.ekn.getpackage.homeassignment.data.local.SessionManager
import com.ekn.getpackage.homeassignment.data.repository.ILoginRepository
import com.ekn.getpackage.homeassignment.data.repository.IMainScreenRepository
import com.ekn.getpackage.homeassignment.data.repository.IVersionRepository
import com.ekn.getpackage.homeassignment.model.VersionInfo
import com.ekn.getpackage.homeassignment.ui.splash.SplashScreenState
import com.ekn.getpackage.homeassignment.ui.splash.SplashViewModel


import io.mockk.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.*


import java.util.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@ExperimentalCoroutinesApi
class SokashViewModelTest {

    private val testDispatcher = TestCoroutineDispatcher()

    @Before
    fun setup() {
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }



    val verRepo = mockk<IVersionRepository>()
    val loginRepo = mockk<ILoginRepository>()
    val mainREpo = mockk<IMainScreenRepository>()
    val sessionManager = mockk<SessionManager>()

    val splashVM = SplashViewModel(verRepo,sessionManager)



    @Test
    fun `version test pass and move to login`() = testDispatcher.runBlockingTest {
        coEvery { sessionManager.getToken() } returns ""
        coEvery { verRepo.getActualAppVersion() } returns VersionInfo.Data(2)
        splashVM.performInit()
        delay(1000)
        coVerify { verRepo.getActualAppVersion() }
        Assert.assertEquals( SplashScreenState.SHOW_LOGIN_SCREEN,splashVM.screenState.value)
    }

    @Test
    fun `version test not pass and show error`()= testDispatcher.runBlockingTest {
        coEvery { sessionManager.getToken() } returns ""
        coEvery { verRepo.getActualAppVersion() } returns VersionInfo.Data(-1)
        splashVM.performInit()
        delay(1000)
        coVerify { verRepo.getActualAppVersion() }
        Assert.assertEquals(SplashScreenState.SHOW_VERSSION_ERROR_DIALOG,splashVM.screenState.value)
    }

    @Test
    fun `version test  pass and show main screen`()= testDispatcher.runBlockingTest {
        coEvery { sessionManager.getToken() } returns UUID.randomUUID().toString()
        coEvery { verRepo.getActualAppVersion() } returns VersionInfo.Data(2)
        splashVM.performInit()
        delay(1000)
        coVerify { verRepo.getActualAppVersion() }
        Assert.assertEquals(SplashScreenState.SHOW_MAIN_SCREEN,splashVM.screenState.value)
    }

}