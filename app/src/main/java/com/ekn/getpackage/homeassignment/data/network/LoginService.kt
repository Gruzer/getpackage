package com.ekn.getpackage.homeassignment.data.network

import com.ekn.getpackage.homeassignment.data.network.contract.ILoginService
import com.ekn.getpackage.homeassignment.model.AuthResult
import java.util.*
import kotlin.random.Random

class LoginService : ILoginService {
    override suspend fun auth(email: String, password: String): NetworkResponse<AuthResult> {
        val num = Random.nextInt(0, 100)
        return if (num > 20) {
            NetworkResponse.Success(AuthResult(UUID.randomUUID().toString()))
        } else {
            NetworkResponse.Failure(500, " Internal Server Error")
        }

    }
}