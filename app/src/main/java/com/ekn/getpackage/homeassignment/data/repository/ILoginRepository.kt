package com.ekn.getpackage.homeassignment.data.repository

import com.ekn.getpackage.homeassignment.model.AuthInfo

interface ILoginRepository {
    suspend fun auth(email: String, password: String): AuthInfo
}