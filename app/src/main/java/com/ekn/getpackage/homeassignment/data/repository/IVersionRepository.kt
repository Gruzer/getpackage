package com.ekn.getpackage.homeassignment.data.repository

import com.ekn.getpackage.homeassignment.model.VersionInfo

interface IVersionRepository {
    suspend fun getActualAppVersion():VersionInfo
}