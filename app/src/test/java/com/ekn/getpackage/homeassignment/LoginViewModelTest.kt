package com.ekn.getpackage.homeassignment

import com.ekn.getpackage.homeassignment.data.local.SessionManager
import com.ekn.getpackage.homeassignment.data.repository.ILoginRepository
import com.ekn.getpackage.homeassignment.data.repository.IMainScreenRepository
import com.ekn.getpackage.homeassignment.data.repository.IVersionRepository
import com.ekn.getpackage.homeassignment.model.AuthInfo
import com.ekn.getpackage.homeassignment.model.VersionInfo
import com.ekn.getpackage.homeassignment.ui.login.LoginScreenIntent
import com.ekn.getpackage.homeassignment.ui.login.LoginScreenState
import com.ekn.getpackage.homeassignment.ui.login.LoginViewModel
import com.ekn.getpackage.homeassignment.ui.splash.SplashScreenState
import com.ekn.getpackage.homeassignment.ui.splash.SplashViewModel
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.util.*


@ExperimentalCoroutinesApi
class LoginViewModelTest {


    private val testDispatcher = TestCoroutineDispatcher()

    @Before
    fun setup() {
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }



    val loginRepo = mockk<ILoginRepository>()
    val sessionManager = mockk<SessionManager>()

    val loginVM = LoginViewModel(loginRepo)



    @Test
    fun `login test pass`() = testDispatcher.runBlockingTest {
        coEvery { loginRepo.auth("","") } returns AuthInfo.Data(UUID.randomUUID().toString(),System.currentTimeMillis())
        loginVM.obtainIntent(LoginScreenIntent.Auth("",""))
        delay(1000)
        coVerify { loginRepo.auth("","") }
        Assert.assertEquals( LoginScreenState.NAVIGATE_MAIN_SCREEN,loginVM.screenState.value)
    }

    @Test
    fun `login test fail`() = testDispatcher.runBlockingTest {
        coEvery { loginRepo.auth("","") } returns AuthInfo.Error("fail",1)
        loginVM.obtainIntent(LoginScreenIntent.Auth("",""))
        delay(1000)
        coVerify { loginRepo.auth("","") }
        Assert.assertEquals(LoginScreenState.Error("fail"),loginVM.screenState.value)
    }

}