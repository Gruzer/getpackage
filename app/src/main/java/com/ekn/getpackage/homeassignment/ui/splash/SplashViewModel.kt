package com.ekn.getpackage.homeassignment.ui.splash

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ekn.getpackage.homeassignment.data.local.ISessionManager
import com.ekn.getpackage.homeassignment.data.repository.IVersionRepository
import com.ekn.getpackage.homeassignment.model.VersionInfo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(
    private val versionRepository: IVersionRepository,
    private val sessionManager: ISessionManager
) : ViewModel() {

    private val _screenState: MutableStateFlow<SplashScreenState> =
        MutableStateFlow(SplashScreenState.NONE)
    val screenState: StateFlow<SplashScreenState>
        get() = _screenState


    fun performInit() {
        checkVersion()
    }

    private fun checkVersion() {
        if (_screenState.value != SplashScreenState.WAIT)
            viewModelScope.launch {
                _screenState.value = SplashScreenState.WAIT
                handleResult(versionRepository.getActualAppVersion())

            }
    }

    private fun handleResult(info: VersionInfo) {
        _screenState.value = SplashScreenState.READY
        when (info) {
            is VersionInfo.Data -> handleVersion(info)
            is VersionInfo.Error -> showError(info)
        }

    }

    private fun handleVersion(info: VersionInfo.Data) {
        if (info.version < 0) {
            _screenState.value = SplashScreenState.SHOW_VERSSION_ERROR_DIALOG
        } else {
            checkSession()
        }
    }

    private fun showError(error: VersionInfo.Error) {
        _screenState.value = SplashScreenState.NetworkError(error.message)
    }

    private fun checkSession() {
        viewModelScope.launch {
            if (sessionManager.getToken().isEmpty())
                showLoginScreen()
            else {
                showMainScreen()
            }
        }

    }

    private fun showLoginScreen() {
        _screenState.value = SplashScreenState.SHOW_LOGIN_SCREEN
    }

    private fun showMainScreen() {
        _screenState.value = SplashScreenState.SHOW_MAIN_SCREEN
    }


}