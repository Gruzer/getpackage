package com.ekn.getpackage.homeassignment.data.network.contract

import com.ekn.getpackage.homeassignment.data.network.NetworkResponse
import com.ekn.getpackage.homeassignment.model.AuthResult

interface ILoginService {
    suspend fun auth(email: String, password: String): NetworkResponse<AuthResult>
}