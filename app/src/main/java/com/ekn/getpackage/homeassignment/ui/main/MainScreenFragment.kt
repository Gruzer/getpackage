package com.ekn.getpackage.homeassignment.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.ekn.getpackage.homeassignment.databinding.FragmentMainScreenBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class MainScreenFragment : Fragment() {

    val mainScreenViewModel: MainScreenViewModel by viewModels()
    private lateinit var binding: FragmentMainScreenBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentMainScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lifecycleScope.launchWhenStarted {
            mainScreenViewModel.screenState.collect {
                render(it)
            }
        }

        binding.mainScreenLogOutBtn.setOnClickListener {
            mainScreenViewModel.obtainIntent(MainScreenIntent.LOGOUT)
        }

    }

    private fun render(state: MainScreenState) {
        when (state) {
            MainScreenState.DONE -> setElementsEnableState(true)
            is MainScreenState.Error -> showError(state.message)
            MainScreenState.IN_PROGRESS -> setElementsEnableState(false)
            MainScreenState.NONE -> setElementsEnableState(false)
            is MainScreenState.SessionInfo -> showSessionInfo(state.time)
            MainScreenState.LOGOUT -> navigateToLoginScreen()
        }
    }

    private fun showError(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }


    private fun setElementsEnableState(enable: Boolean) {
        binding.mainScreenLastLoginTxt.isEnabled = enable
        binding.mainScreenLogOutBtn.isEnabled = enable
    }

    private fun navigateToLoginScreen() {
        findNavController().navigate(MainScreenFragmentDirections.actionMainScreenFragmentToLoginFragment())
    }

    private fun showSessionInfo(time: String) {
        binding.mainScreenLastLoginTxt.text = time
    }


    companion object {

        @JvmStatic
        fun newInstance() =
            MainScreenFragment()

    }
}