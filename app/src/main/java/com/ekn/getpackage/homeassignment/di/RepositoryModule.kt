package com.ekn.getpackage.homeassignment.di

import android.content.Context
import com.ekn.getpackage.homeassignment.data.local.ISessionManager
import com.ekn.getpackage.homeassignment.data.local.SessionManager
import com.ekn.getpackage.homeassignment.data.network.LoginService
import com.ekn.getpackage.homeassignment.data.network.VersionService
import com.ekn.getpackage.homeassignment.data.network.contract.ILoginService
import com.ekn.getpackage.homeassignment.data.network.contract.IVersionService
import com.ekn.getpackage.homeassignment.data.repository.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent


@InstallIn(ViewModelComponent::class)
@Module
object RepositoryModule {

    @Provides
    fun provideLoginRepository(
        service: ILoginService,
        sessionManager: ISessionManager
    ): ILoginRepository {
        return LoginRepository(service, sessionManager)
    }


    @Provides
    fun provideVersionRepository(service: IVersionService): IVersionRepository {
        return VersionRepository(service)
    }

    @Provides
    fun provideMainScreenRepository(sessionManager: ISessionManager): IMainScreenRepository {
        return MainScreenRepository(sessionManager)
    }

    @Provides
    fun provideLoginService(): ILoginService {
        return LoginService()
    }

    @Provides
    fun provideVersionService(): IVersionService {
        return VersionService()
    }

    @Provides
    fun provideSessionManger(@ApplicationContext context: Context): ISessionManager {
        return SessionManager(context)
    }
}